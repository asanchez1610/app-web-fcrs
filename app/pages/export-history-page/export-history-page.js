import { html } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import {
  MasterPage,
  gridStyles,
  stylesPages,
} from '../../elements/utils/master-page.js';
import '@bbva-web-components/bbva-core-moment-import/lib/bbva-core-moment-import.min.js';
import '@bbva-web-components/bbva-web-form-date/bbva-web-form-date.js';
import '@bbva-web-components/bbva-button-default/bbva-button-default';
import styles from './export-history-styles.js';

class ExportHistoryPage extends MasterPage {
  static get is() {
    return 'export-history-page';
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  firstUpdated() {
    this.element('#fIni').value = `${this.mmt()
      .subtract(6, 'months')
      .format('YYYY-MM-DD')}`;
    this.element('#fFin').value = `${this.mmt().format('YYYY-MM-DD')}`;
  }

  initExport() {
    console.log('initExport');
    const fIni = this.element('#fIni').value;
    const fFin = this.element('#fFin').value;
    const rangeDays = 15;
    let dateInit = this.mmt(fIni);
    let dateInitStr = this.mmt(dateInit).format('YYYY-MM-DD');
    let dateFin = this.mmt(fIni).add(rangeDays, 'days');
    let dateFinStr = dateFin.format('YYYY-MM-DD');
    let dateEnd = this.mmt(fFin);
    let count = 1;
    let tmpFinNext = null;
    let tmpFin = null;

    let path = `${this.appProps.services.paths.issueTracker}?isSmartAssistant=true&fromCreationDate={0}&toCreationDate={1}`;

    let dataRequest = {
      host: this.dataAccess.host,
      path: path.replace('{0}', dateInitStr).replace('{1}', dateFinStr),
      onSuccess: (serverResult) => {
        console.log('onSuccess', serverResult);
      },
      onError: (error) => {
        console.log('onError', error);
      },
    };
    this.dispatch('send-request-dp', dataRequest);
    while (!dateFin.isAfter(dateEnd)) {
      tmpFin = this.mmt(dateFin);
      dateInit = tmpFin.add(1, 'days');
      tmpFinNext = this.mmt(dateInit);
      dateFin = tmpFinNext.add(rangeDays, 'days');
      dateInitStr = dateInit.format('YYYY-MM-DD');
      dateFinStr = tmpFinNext.format('YYYY-MM-DD');
      count = count + 1;
      dataRequest.path = path
        .replace('{0}', dateInitStr)
        .replace('{1}', dateFinStr);
      this.dispatch('send-request-dp', dataRequest);
    }
    console.log(count);
  }

  render() {
    return this.content(html`
      <h2 class="title-page">Exportación de FCR's</h2>

      <div class="row">
        <div
          class="col-12	col-sm-5 col-md-5 col-lg-4 col-xl-4	col-xxl-3 col-section"
        >
          <bbva-web-form-date
            id="fIni"
            label="Fecha inicio"
            value="2020-06-24"
          ></bbva-web-form-date>
          <bbva-web-form-date
            id="fFin"
            label="Fecha fin"
            value="2020-06-24"
          ></bbva-web-form-date>
          <bbva-button-default
            text="Exportar"
            @click="${this.initExport}"
          ></bbva-button-default>
        </div>
        <div
          class="col-12	col-sm-7 col-md-7 col-lg-8 col-xl-8	col-xxl-9 col-section"
        >
          Hola
        </div>
      </div>
    `);
  }

  get mmt() {
    return window.moment;
  }

  onPageEnter() {
    console.log('existe moment (mmt)', this.mmt);
  }

  onPageLeave() {}

  static get styles() {
    return [stylesPages, gridStyles, styles];
  }
}

window.customElements.define(ExportHistoryPage.is, ExportHistoryPage);
