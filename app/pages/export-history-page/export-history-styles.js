import { css } from 'lit-element';
export default css`
:host {
  display: block;
  box-sizing: border-box;
}

:host([hidden]),
[hidden] {
  display: none !important;
}

bbva-web-form-date {
  margin-bottom: 10px;
}

bbva-button-default {
  width: 100%;
  max-width: 100%;
}`;
