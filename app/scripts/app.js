(function() {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'init': '/',
      'export-history': '/export-history'
    }
  });
}());
